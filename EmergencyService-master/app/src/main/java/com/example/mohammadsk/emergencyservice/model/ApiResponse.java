package com.example.mohammadsk.emergencyservice.model;

import java.util.List;

public class ApiResponse {

    private int code;
    private String message;
    private boolean error;
    private List<Services> services;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public boolean isError() {
        return error;
    }

    public List<Services> getServices() {
        return services;
    }

}
