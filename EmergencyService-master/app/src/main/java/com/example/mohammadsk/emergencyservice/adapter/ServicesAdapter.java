package com.example.mohammadsk.emergencyservice.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mohammadsk.emergencyservice.model.Services;
import com.example.mohammadsk.emergencyservice.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MohammadSk on 12-Nov-17.
 */

public class ServicesAdapter extends ArrayAdapter<Services> {

    private Context mContext;
    private int mResource;
    private int lastPosition = -1;

    private List<Services> mObjects = null;
    private ArrayList<Services> arrayList = new ArrayList<>();

    static class ViewHolder {
        TextView sName;
        TextView sMobileNo;
    }

    public ServicesAdapter(@NonNull Context context, int resource, @NonNull List<Services> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        mObjects = objects;

        arrayList.addAll(objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final String name = getItem(position).getName();
        final String mobileNo = getItem(position).getMobileNo();
        final String address = getItem(position).getAddress();
        final String zone = getItem(position).getZone();
        final String district = getItem(position).getDistrict();
        final String latitude = getItem(position).getLat();
        final String longitude = getItem(position).getLongi();

        Services services = new Services(name, mobileNo, address, zone, district, latitude, longitude);

        ViewHolder holder;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.custom_list_item, parent, false);

        holder = new ViewHolder();

        holder.sName = convertView.findViewById(R.id.custom_name_field);
        holder.sMobileNo = convertView.findViewById(R.id.custom_phoneNumber);


        convertView.setTag(holder);

        holder.sName.setText(services.getName());
        holder.sMobileNo.setText(services.getMobileNo());

        return convertView;
    }


    // Filter Class
    public void filter(String searchZone) {
        String sZone = searchZone.toLowerCase();
        List<Services> newList = new ArrayList<>();
        mObjects.clear();

        if (sZone.length() == 0) {
            mObjects.addAll(arrayList);
        } else {
            for (Services searchServices : arrayList) {
                String hZone = searchServices.getZone().toLowerCase();
                if (hZone.contains(sZone)){
                    newList.add(searchServices);
                }
            }
            mObjects.addAll(newList);
        }
        notifyDataSetChanged();
    }

}
