package com.example.mohammadsk.emergencyservice.interface_api_call;

import com.example.mohammadsk.emergencyservice.model.ApiResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by MohammadSk on 17-Nov-17.
 */

public interface ApiInterface {


    @POST("hospital.php")
    @FormUrlEncoded
    Call<ApiResponse> getHospitalList(@Field("auth_key") String id);

    @POST("police_station.php")
    @FormUrlEncoded
    Call<ApiResponse> getPoliceStationList(@Field("auth_key") String id);

    @POST("fire_service.php")
    @FormUrlEncoded
    Call<ApiResponse> getFireServiceList(@Field("auth_key") String id);

}
