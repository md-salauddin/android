package com.example.mohammadsk.emergencyservice.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mohammadsk.emergencyservice.R;

public class CustomDialog {

    private Activity activity;

    public CustomDialog(Activity activity) {
        this.activity = activity;
    }

    public void showDialog(String title, String address, int image,
                           final String mobileNo,
                           final String latitude,
                           final String longitude){

        final Dialog dialog = new Dialog(activity);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        TextView titleTxtView = dialog.findViewById(R.id.title_name_dia_txt_view);
        TextView addressTxtView = dialog.findViewById(R.id.address_dia_txt_view);
        TextView mobileNoTxtView = dialog.findViewById(R.id.mobile_no_dia_txt_view);
        ImageView logo = dialog.findViewById(R.id.dia_img_view);

        titleTxtView.setText(title);
        addressTxtView.setText(address);
        mobileNoTxtView.setText(mobileNo);
        logo.setImageResource(image);

        Button closeButton = dialog.findViewById(R.id.close_dia_btn);
        ImageView showMap = dialog.findViewById(R.id.show_dia_btn);
        ImageView call = dialog.findViewById(R.id.call_dia_btn);

        // dismiss dialog
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // show into google map
        showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIntoMap(latitude, longitude);
            }
        });

        // call into certain number
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callUser(mobileNo);
            }
        });

        dialog.show();

    }

    // make a call
    public void callUser(String phoneNumber){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phoneNumber));
        activity.startActivity(intent);
    }

    // show into google map
    public void showIntoMap(String latitude, String longitude){
        Intent intent, selector;
        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<"+latitude +">,<"+longitude+">?q=<"+latitude +">,<"+longitude+">"));
        selector = Intent.createChooser(intent, "Select Map Launcher"); // it will create a option if don't have google map
        activity.startActivity(selector);
    }

}
