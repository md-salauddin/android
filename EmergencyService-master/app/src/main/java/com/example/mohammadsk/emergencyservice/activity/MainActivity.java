package com.example.mohammadsk.emergencyservice.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.mohammadsk.emergencyservice.R;


public class MainActivity extends AppCompatActivity {


    private static final long SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                startActivity(new Intent(MainActivity.this, DashboardActivity.class));
                finish();

            }
        }, SPLASH_TIME_OUT);

    }



}
