package com.example.mohammadsk.emergencyservice.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mohammadsk.emergencyservice.R;
import com.example.mohammadsk.emergencyservice.adapter.ServicesAdapter;
import com.example.mohammadsk.emergencyservice.dialog.CustomDialog;
import com.example.mohammadsk.emergencyservice.interface_api_call.ApiInterface;
import com.example.mohammadsk.emergencyservice.model.ApiResponse;
import com.example.mohammadsk.emergencyservice.model.Services;
import com.example.mohammadsk.emergencyservice.rertrofit.ApiClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MohammadSk on 24-Oct-17.
 */

public class FireServiceFragment extends Fragment implements
        Callback<ApiResponse>,
        AdapterView.OnItemClickListener{

    private ListView listView;
    private ApiInterface apiInterface;
    private ApiResponse apiResponse = new ApiResponse();
    private ArrayList<Services> services;
    private Services service;
    private ServicesAdapter servicesAdapter;

    private ProgressBar servicesProgressBar;
    private TextView dataTextView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_services_layout, container, false);
        listView = view.findViewById(R.id.services_list_view);
        servicesProgressBar = view.findViewById(R.id.services_progress_bar);
        dataTextView = view.findViewById(R.id.services_txt_view);
        services = new ArrayList<>();

        // create interface instance object
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        Call<ApiResponse> call = apiInterface.getFireServiceList(getString(R.string.auth_key));
        call.enqueue(this);

        // set onclick listener when a list view being pressed
        listView.setOnItemClickListener(this);

        return view;

    }


    // show into list view
    public void showIntoListView() {

        for (int i = 0; i < apiResponse.getServices().size(); i++) {
            String name = apiResponse.getServices().get(i).getName();
            String mobileNo = apiResponse.getServices().get(i).getMobileNo();
            String address = apiResponse.getServices().get(i).getAddress();
            String zone = apiResponse.getServices().get(i).getZone();
            String district = apiResponse.getServices().get(i).getDistrict();
            String latitude = apiResponse.getServices().get(i).getLat();
            String longitude = apiResponse.getServices().get(i).getLongi();

            service = new Services(name, mobileNo, address, zone, district, latitude, longitude);
            services.add(service);
        }
        servicesAdapter = new ServicesAdapter(getContext(), R.layout.custom_list_item, services);
        listView.setAdapter(servicesAdapter);
    }

    @Override
    public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
        if (response.isSuccessful()) {
            ApiResponse apiResponseValue = response.body();

            if (apiResponseValue.getCode() == 200) {
                apiResponse = apiResponseValue;
                showIntoListView();
                servicesProgressBar.setVisibility(View.INVISIBLE);
                Log.i("services", "data found");

            }
            else {
                servicesProgressBar.setVisibility(View.INVISIBLE);
                dataTextView.setVisibility(View.VISIBLE);
                Log.i("services", "data not found");
            }
        }
    }

    @Override
    public void onFailure(Call<ApiResponse> call, Throwable t) {
        servicesProgressBar.setVisibility(View.INVISIBLE);
        dataTextView.setVisibility(View.VISIBLE);
        Log.i("services", "error :"+t.getMessage());
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        String name = services.get(i).getName();
        String mobileNo = services.get(i).getMobileNo();
        String address = services.get(i).getAddress();
        String zone = services.get(i).getZone();
        String district = services.get(i).getDistrict();
        String lat = services.get(i).getLat();
        String longi = services.get(i).getLongi();

        CustomDialog customDialog = new CustomDialog(getActivity());
        customDialog.showDialog(name,
                address+"\n"+zone+"\n"+district,
                R.drawable.ic_fire_service,
                mobileNo,
                lat,
                longi);
    }



}