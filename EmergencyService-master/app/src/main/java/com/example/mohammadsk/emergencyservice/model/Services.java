package com.example.mohammadsk.emergencyservice.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MohammadSk on 12-Nov-17.
 */

public class Services {

    @SerializedName("name")
    private String name;
    @SerializedName("phoneNumber")
    private String mobileNo;
    @SerializedName("address")
    private String address;
    @SerializedName("zone")
    private String zone;
    @SerializedName("district")
    private String district;
    @SerializedName("latitude")
    private String lat;
    @SerializedName("longitude")
    private String longi;


    public Services(String name, String mobileNo, String address, String zone, String district, String lat, String longi) {
        this.name = name;
        this.mobileNo = mobileNo;
        this.address = address;
        this.zone = zone;
        this.district = district;
        this.lat = lat;
        this.longi = longi;

    }

    public String getName() {
        return name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getAddress() {
        return address;
    }

    public String getZone() {
        return zone;
    }

    public String getDistrict() {
        return district;
    }

    public String getLat() {
        return lat;
    }

    public String getLongi() {
        return longi;
    }


    @Override
    public String toString() {
        return  name +"\n"+
                mobileNo +"\n\n"+
                address +"\n\n"+
                zone +"\n"+
                district +"\n";
    }

}
