package com.sktechbd.xyz.movies.popular_movies.presenter;

import android.app.Activity;
import android.util.Log;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sktechbd.xyz.movies.api.MovieDBClient;
import com.sktechbd.xyz.movies.popular_movies.model.PopularMovie;
import com.sktechbd.xyz.movies.popular_movies.model.PopularMovieResult;
import com.sktechbd.xyz.movies.popular_movies.view.PopularMovieInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sktechbd.xyz.movies.utiles.ConstantFile.API_KEY;

public class PopularMoviePresenter {

    private PopularMovieInterface popularMovieInterface;
    private Activity activity;
    private int pageNumber;

    private boolean isScrolled = false, isAllDataFetched = false;
    private int currentItem, scrollOutItem, totalItem;
    private List<PopularMovie> popularMovies = new ArrayList<>();

    public PopularMoviePresenter(PopularMovieInterface popularMovieInterface, Activity activity, int pageNumber) {
        this.popularMovieInterface = popularMovieInterface;
        this.activity = activity;
        this.pageNumber = pageNumber;
    }

    public void getPopularMovies(boolean isFirstCall) {
        if (isFirstCall)
            popularMovieInterface.showProgressBar();
        else
            popularMovieInterface.showPagingProgressBar();

        Call<PopularMovieResult> call = MovieDBClient
                .getInstance()
                .getApi()
                .getPopularMovies(API_KEY, pageNumber);

        call.enqueue(new Callback<PopularMovieResult>() {
            @Override
            public void onResponse(Call<PopularMovieResult> call, Response<PopularMovieResult> response) {
                if (response.isSuccessful() && response.body() != null) {
                    PopularMovieResult popularMovieResult = response.body();
                    popularMovies.addAll(popularMovieResult.getResults());
                    popularMovieInterface.getPopularMovies(popularMovies, isFirstCall);

                    if (pageNumber == popularMovieResult.getTotalPage())
                        isAllDataFetched = true;

                    pageNumber++;
                    Log.i("MainActivity", "Page number: "+pageNumber+
                            "\t Page remaining: "+(popularMovieResult.getTotalPage()-popularMovieResult.getPageNumber())+
                            "\t List size: "+popularMovies.size());
                }
                else {
                    popularMovieInterface.showErrorMessage();
                }
                popularMovieInterface.hideProgressBar();
                popularMovieInterface.hidePagingProgressBar();
            }

            @Override
            public void onFailure(Call<PopularMovieResult> call, Throwable t) {
                popularMovieInterface.showErrorMessage();
                popularMovieInterface.hideProgressBar();
                popularMovieInterface.hidePagingProgressBar();
            }
        });
    }

    public void getPaginationList(RecyclerView recyclerView, GridLayoutManager gridLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItem = gridLayoutManager.getChildCount();
                totalItem = gridLayoutManager.getItemCount();
                scrollOutItem = gridLayoutManager.findFirstVisibleItemPosition();

                if (isScrolled && (totalItem == (currentItem+scrollOutItem))) {
                    isScrolled = false;
                    if (!isAllDataFetched)
                        getPopularMovies(false);
                }

            }
        });
    }

}
