package com.sktechbd.xyz.movies.api;

import com.sktechbd.xyz.movies.popular_movies.model.PopularMovieResult;
import com.sktechbd.xyz.movies.single_movie_details.model.MovieDetails;
import com.sktechbd.xyz.movies.utiles.ConstantFile;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieDBInterface {

    @GET(ConstantFile.GET_SINGLE_MOVIE)
    Call<MovieDetails> getSingleMovieDetails(
            @Path("movie_id") long id,
            @Query("api_key") String apiKey
    );

    @GET(ConstantFile.GET_POPULAR_MOVIES)
    Call<PopularMovieResult> getPopularMovies(
            @Query("api_key") String apiKey,
            @Query("page") int pageNumber
    );

}
