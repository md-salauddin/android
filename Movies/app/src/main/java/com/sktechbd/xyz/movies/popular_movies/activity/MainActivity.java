package com.sktechbd.xyz.movies.popular_movies.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sktechbd.xyz.movies.R;
import com.sktechbd.xyz.movies.popular_movies.adapter.PopularMovieAdapter;
import com.sktechbd.xyz.movies.popular_movies.model.PopularMovie;
import com.sktechbd.xyz.movies.popular_movies.presenter.PopularMoviePresenter;
import com.sktechbd.xyz.movies.popular_movies.view.PopularMovieInterface;

import java.util.List;


public class MainActivity extends AppCompatActivity implements PopularMovieInterface {

    private PopularMoviePresenter popularMoviePresenter;
    private PopularMovieAdapter popularMovieAdapter;
    private TextView tvErrorMessage;
    private ProgressBar pbPopularMovie, pbLoading;
    private RecyclerView rvPopularMovie;
    private GridLayoutManager gridLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        gridLayoutManager = new GridLayoutManager(this, 2);
        popularMoviePresenter = new PopularMoviePresenter(this, MainActivity.this, 1);
        popularMoviePresenter.getPopularMovies(true);
        popularMoviePresenter.getPaginationList(rvPopularMovie, gridLayoutManager);

    }

    private void init() {
        pbPopularMovie = findViewById(R.id.pb_popular_movie);
        pbLoading = findViewById(R.id.pb_popular_loading);
        tvErrorMessage = findViewById(R.id.tv_popular_no_connection);
        rvPopularMovie = findViewById(R.id.rv_popular_movie);
    }


    @Override
    public void getPopularMovies(List<PopularMovie> popularMovies, boolean isFirstCall) {
        updateUi(popularMovies, isFirstCall);
    }

    @Override
    public void showProgressBar() {
        pbPopularMovie.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbPopularMovie.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showPagingProgressBar() {
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePagingProgressBar() {
        pbLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showErrorMessage() {
        tvErrorMessage.setVisibility(View.VISIBLE);
    }

    private void updateUi(List<PopularMovie> popularMovies, boolean isFirstCall) {
        if (isFirstCall) {
            popularMovieAdapter = new PopularMovieAdapter(this, popularMovies);
            rvPopularMovie.setHasFixedSize(true);
            rvPopularMovie.setLayoutManager(gridLayoutManager);
            //rvPopularMovie.setLayoutManager(new LinearLayoutManager(this));
            rvPopularMovie.setAdapter(popularMovieAdapter);
        }
        else
            popularMovieAdapter.notifyDataSetChanged();
    }

}