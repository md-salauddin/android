package com.sktechbd.xyz.movies.popular_movies.model;

import com.google.gson.annotations.SerializedName;

public class PopularMovie {

    private long id;
    private String title;

    @SerializedName("poster_path")
    private String imageUrl;

    @SerializedName("original_title")
    private String name;

    @SerializedName("release_date")
    private String releaseDate;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

}
