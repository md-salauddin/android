package com.sktechbd.xyz.movies.popular_movies.view;

import com.sktechbd.xyz.movies.popular_movies.model.PopularMovie;
import com.sktechbd.xyz.movies.popular_movies.model.PopularMovieResult;

import java.util.List;


public interface PopularMovieInterface {

    void getPopularMovies(List<PopularMovie> popularMovies, boolean isFirstCall);
    void showProgressBar();
    void hideProgressBar();
    void showPagingProgressBar();
    void hidePagingProgressBar();
    void showErrorMessage();

}
