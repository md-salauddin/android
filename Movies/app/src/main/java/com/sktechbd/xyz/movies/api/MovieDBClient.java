package com.sktechbd.xyz.movies.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.sktechbd.xyz.movies.utiles.ConstantFile.BASE_URL;

public class MovieDBClient {

    private static MovieDBClient movieDBClient;
    private Retrofit retrofit;

    private MovieDBClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized MovieDBClient getInstance() {
        if (movieDBClient == null )
            movieDBClient = new MovieDBClient();
        return movieDBClient;
    }

    public MovieDBInterface getApi() {
        return retrofit.create(MovieDBInterface.class);
    }


}
