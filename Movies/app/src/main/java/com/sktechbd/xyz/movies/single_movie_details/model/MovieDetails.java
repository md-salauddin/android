package com.sktechbd.xyz.movies.single_movie_details.model;

import com.google.gson.annotations.SerializedName;

public class MovieDetails {

    private long id;
    private String overview;
    private double popularity;
    private int revenue;
    private int budget;
    private int runtime;
    private String status;
    private String title;

    @SerializedName("original_title")
    private String name;

    @SerializedName("poster_path")
    private String posterUrl;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("vote_average")
    private float rating;

    public long getId() {
        return id;
    }

    public String getOverview() {
        return overview;
    }

    public String getTitle() {
        return title;
    }

    public double getPopularity() {
        return popularity;
    }

    public int getRevenue() {
        return revenue;
    }

    public int getBudget() {
        return budget;
    }

    public int getRuntime() {
        return runtime;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public float getRating() {
        return rating;
    }



}
