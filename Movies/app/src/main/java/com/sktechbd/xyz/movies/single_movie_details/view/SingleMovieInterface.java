package com.sktechbd.xyz.movies.single_movie_details.view;

import com.sktechbd.xyz.movies.single_movie_details.model.MovieDetails;

public interface SingleMovieInterface {
    void getSingleMovie(MovieDetails movieDetails);
    void showProgressBar();
    void hideProgressBar();
    void showErrorMessage();
}
