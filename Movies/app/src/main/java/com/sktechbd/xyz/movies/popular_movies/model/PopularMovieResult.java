package com.sktechbd.xyz.movies.popular_movies.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PopularMovieResult {

    @SerializedName("page")
    private int pageNumber;

    @SerializedName("total_results")
    private long totalResults;

    @SerializedName("total_pages")
    private int totalPage;

    private List<PopularMovie> results;

    public int getPageNumber() {
        return pageNumber;
    }

    public long getTotalResults() {
        return totalResults;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public List<PopularMovie> getResults() {
        return results;
    }


}
