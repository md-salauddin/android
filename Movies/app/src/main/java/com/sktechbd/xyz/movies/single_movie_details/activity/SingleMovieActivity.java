package com.sktechbd.xyz.movies.single_movie_details.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sktechbd.xyz.movies.R;
import com.sktechbd.xyz.movies.single_movie_details.model.MovieDetails;
import com.sktechbd.xyz.movies.single_movie_details.presenter.SingleMoviePresenter;
import com.sktechbd.xyz.movies.single_movie_details.view.SingleMovieInterface;

import static com.sktechbd.xyz.movies.utiles.ConstantFile.IMAGE_BASE_URL;
import static com.sktechbd.xyz.movies.utiles.ConstantFile.MOVIE_ID;


public class SingleMovieActivity extends AppCompatActivity implements SingleMovieInterface {

    private SingleMoviePresenter singleMoviePresenter;

    private LinearLayout llMainContainer;
    private ProgressBar pbSingleMovie;
    private TextView tvErrorMessage;
    private TextView tvMovieName, tvTitle, tvOverView;
    private TextView tvReleaseDate, tvRating, tvRuntime, tvRevenue, tvBudget;
    private ImageView imgPoster;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_movie);

        init();

        Intent intent = getIntent();
        long movieId = intent.getLongExtra(MOVIE_ID, 0);

        singleMoviePresenter = new SingleMoviePresenter(this, SingleMovieActivity.this);
        singleMoviePresenter.getSingleMovie(movieId);

    }


    private void init() {
        llMainContainer = findViewById(R.id.ll_single_movie_container);
        pbSingleMovie = findViewById(R.id.pb_single_movie);
        tvErrorMessage = findViewById(R.id.tv_no_connection);
        tvMovieName = findViewById(R.id.tv_single_movie_name);
        tvTitle = findViewById(R.id.tv_single_movie_short_title);
        tvOverView = findViewById(R.id.tv_single_movie_overview);
        tvReleaseDate = findViewById(R.id.tv_single_movie_release_date);
        tvRating = findViewById(R.id.tv_single_movie_rating);
        tvRuntime = findViewById(R.id.tv_single_movie_runtime);
        tvRevenue = findViewById(R.id.tv_single_movie_revenue);
        tvBudget = findViewById(R.id.tv_single_movie_budget);
        imgPoster = findViewById(R.id.img_single_movie_poster);
    }


    @Override
    public void getSingleMovie(MovieDetails movieDetails) {
        llMainContainer.setVisibility(View.VISIBLE);
        updateUI(movieDetails);
    }

    @Override
    public void showProgressBar() {
        pbSingleMovie.setVisibility(View.VISIBLE);
        llMainContainer.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbSingleMovie.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage() {
        tvErrorMessage.setVisibility(View.VISIBLE);
        pbSingleMovie.setVisibility(View.INVISIBLE);
    }

    private void updateUI(MovieDetails movieDetails) {
        if (movieDetails != null) {
            if(movieDetails.getPosterUrl() != null) {
                Glide.with(this)
                     .load(IMAGE_BASE_URL+movieDetails.getPosterUrl())
                     .into(imgPoster);
            }
            tvMovieName.setText(movieDetails.getName());
            tvTitle.setText(movieDetails.getStatus());
            tvOverView.setText(movieDetails.getOverview());
            tvReleaseDate.setText(movieDetails.getReleaseDate());
            tvRating.setText(String.valueOf(movieDetails.getRating()));
            tvRuntime.setText(String.valueOf(movieDetails.getRuntime()));
            tvRevenue.setText(String.valueOf(movieDetails.getRevenue()));
            tvBudget.setText(String.valueOf(movieDetails.getBudget()));

        }
    }

}