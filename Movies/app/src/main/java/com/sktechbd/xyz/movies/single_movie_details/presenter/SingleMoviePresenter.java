package com.sktechbd.xyz.movies.single_movie_details.presenter;

import android.app.Activity;

import com.sktechbd.xyz.movies.api.MovieDBClient;
import com.sktechbd.xyz.movies.single_movie_details.model.MovieDetails;
import com.sktechbd.xyz.movies.single_movie_details.view.SingleMovieInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sktechbd.xyz.movies.utiles.ConstantFile.API_KEY;

public class SingleMoviePresenter {

    private SingleMovieInterface singleMovieInterface;
    private Activity activity;

    public SingleMoviePresenter(SingleMovieInterface singleMovieInterface, Activity activity) {
        this.singleMovieInterface = singleMovieInterface;
        this.activity = activity;
    }

    public void getSingleMovie(long movieId) {
        singleMovieInterface.showProgressBar();
        Call<MovieDetails> call = MovieDBClient
                .getInstance()
                .getApi()
                .getSingleMovieDetails(movieId, API_KEY);

        call.enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {

                if (response.isSuccessful() && response.body() != null) {
                    MovieDetails movieDetails = response.body();
                    singleMovieInterface.getSingleMovie(movieDetails);
                }
                else {
                    singleMovieInterface.showErrorMessage();
                }

                singleMovieInterface.hideProgressBar();
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {
                singleMovieInterface.showErrorMessage();
                singleMovieInterface.hideProgressBar();
            }
        });
    }




}
