package com.sktechbd.xyz.movies.popular_movies.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sktechbd.xyz.movies.R;
import com.sktechbd.xyz.movies.popular_movies.model.PopularMovie;
import com.sktechbd.xyz.movies.popular_movies.model.PopularMovieResult;
import com.sktechbd.xyz.movies.single_movie_details.activity.SingleMovieActivity;

import java.util.List;

import static com.sktechbd.xyz.movies.utiles.ConstantFile.IMAGE_BASE_URL;
import static com.sktechbd.xyz.movies.utiles.ConstantFile.MOVIE_ID;

public class PopularMovieAdapter extends RecyclerView.Adapter<PopularMovieAdapter.ViewHolder>{

    private Context context;
    private List<PopularMovie> popularMovies;

    public PopularMovieAdapter(Context context, List<PopularMovie> popularMovies) {
        this.context = context;
        this.popularMovies = popularMovies;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_popular_movie, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PopularMovie popularMovie = popularMovies.get(position);

        holder.textView.setText(popularMovie.getName());
        if(popularMovie.getImageUrl() != null) {
            Glide.with(context)
                    .load(IMAGE_BASE_URL+popularMovie.getImageUrl())
                    .into(holder.imageView);
        }

        holder.imageView.setOnClickListener(view -> context.startActivity(new Intent(context, SingleMovieActivity.class)
                .putExtra(MOVIE_ID, popularMovie.getId())));

    }

    @Override
    public int getItemCount() {
        return popularMovies.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.item_img_popular_movie_poster);
            this.textView = itemView.findViewById(R.id.item_tv_popular_movie_name);
        }

    }

}
