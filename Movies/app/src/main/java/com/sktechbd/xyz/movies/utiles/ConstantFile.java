package com.sktechbd.xyz.movies.utiles;

public class ConstantFile {

    // https://api.themoviedb.org/3/movie/popular?api_key=<<api_key>>&page=1
    // https://api.themoviedb.org/3/movie/{movie_id}?api_key=<<api_key>>

    public static  final  String MOVIE_ID = "movie_id";

    public static final String API_KEY = "d3bbf390f933ca5b60bff9a3d3bd002b";
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w342/";

    public static final String GET_SINGLE_MOVIE = "movie/{movie_id}";
    public static final String GET_POPULAR_MOVIES = "movie/popular";
}
